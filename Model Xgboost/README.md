# Analisis Kebiasaan Nasabah

## Getting started
# Setup Variable
PROJECT_ID='wildan-playground-devoteam'
BASE_CPU_IMAGE='asia-docker.pkg.dev/vertex-ai/training/xgboost-cpu.1-1:latest'
OUTPUT_IMAGE=$PROJECT_ID-local-package-cpu:latest
MODEL_DIR='gs://dev-vertex-ai-mlops-model/xgboost-credit-scoring'
DATA_FORMAT='bigquery'
TRAINING='bq://wildan-playground-devoteam.cicil.training'
REGION='asia-southeast2'
DISPLAY='XgBoost-Credit'
IMAGE_NAME='XgBoost'

# The simplest way to package your application and upload it along with its dependencies is to use the gcloud CLI. 
## Auto packaging for testing on local
gcloud ai custom-jobs local-run \
--executor-image-uri=$BASE_CPU_IMAGE \
--python-module=app \
--output-image-uri=$OUTPUT_IMAGE \
-- \
--training_data_uri=$TRAINING \
--data_format=$DATA_FORMAT \
--model_dir=$MODEL_DIR 

## Auto packaging for running custom job
gcloud ai custom-jobs create \
--region=$REGION \
--display-name=$DISPLAY \
--args=--training_data_uri=$TRAINING \
--args=--data_format=$DATA_FORMAT \
--args=--model_dir=$MODEL_DIR \
--worker-pool-spec=machine-type=n1-standard-4,replica-count=1,executor-image-uri=$BASE_CPU_IMAGE,local-package-path=xgboost,python-module=app

# Push container to Artifact Registry
PROJECT_ID='wildan-playground-devoteam'
IMAGE_NAME='xgboost-credit'
REGION='asia-southeast2'
IMAGE_URI=$REGION-docker.pkg.dev/$PROJECT_ID/credit-scoring/$IMAGE_NAME

# build your container by running the following:
docker build ./ -t $IMAGE_URI

# Finally, push the container to Artifact Registry:
docker push $IMAGE_URI