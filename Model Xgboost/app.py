from google.cloud import bigquery, bigquery_storage, storage
import os, logging, json, pickle, argparse
import dask.dataframe as dd
from typing import Union
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import xgboost as xgb

#Import Data Train from GCS / BQ to Dataframe
def load_data_from_gcs(data_gcs_path: str) -> pd.DataFrame:
    '''
    Loads data from Google Cloud Storage (GCS) to a dataframe

            Parameters:
                    data_gcs_path (str): gs path for the location of the data. Wildcards are also supported. i.e gs://example_bucket/data/training-*.csv

            Returns:
                    pandas.DataFrame: a dataframe with the data from GCP loaded
    '''
        
    # using dask that supports wildcards to read multiple files. Then with dd.read_csv().compute we create a pandas dataframe 
    # We will later fix this upnormality
    logging.info("reading gs data: {}".format(data_gcs_path))

    data = dd.read_csv(data_gcs_path)
    data = data.set_index("timestamp") 
    data = data.compute()

    # transform data file index to datetime and sort in chronological order
    data.index = pd.to_datetime(data.index)
    data = data.sort_index()
    print("Dataset shape:", data.shape)

    return data

def load_data_from_bq(bq_uri: str) -> pd.DataFrame:
    '''
    Loads data from BigQuery table (BQ) to a dataframe

            Parameters:
                    bq_uri (str): bq table uri. i.e: example_project.example_dataset.example_table
            Returns:
                    pandas.DataFrame: a dataframe with the data from GCP loaded
    '''
    if not bq_uri.startswith('bq://'):
        raise Exception("uri is not a BQ uri. It should be bq://project_id.dataset.table")
    logging.info("reading bq data: {}".format(bq_uri))
    project,dataset,table =  bq_uri.split(".")
    bqclient = bigquery.Client(project=project[5:])
    bqstorageclient = bigquery_storage.BigQueryReadClient()
    query_string = """
    SELECT * from {ds}.{tbl}
    """.format(ds=dataset, tbl=table)

    return (
        bqclient.query(query_string)
        .result()
        .to_dataframe(bqstorage_client=bqstorageclient)
    )

#Train Model
def train_model(params_IF: dict, df: Union[pd.DataFrame, np.ndarray]) -> xgb.XGBClassifier:
    '''
    Builds and Train a sklearn Isolation Forest Model
    
            Parameters:
                    X: (pd.DataFrame OR np.ndarray): Training vectors of shape n_samples x n_features, where n_samples is the number of samples and n_features is the number of features.

            Returns:
                    Model: sklearn Isolation Forest Model Training
    '''

    y = df.loc[:,df.columns.isin(['SeriousDlqin2yrs'])]
    X_attributes=[
        'RevolvingUtilizationOfUnsecuredLines', 'age',
        'NumberOfTime30-59DaysPastDueNotWorse', 'DebtRatio', 'MonthlyIncome',
        'NumberOfOpenCreditLinesAndLoans', 'NumberOfTimes90DaysLate',
        'NumberRealEstateLoansOrLines',
        'NumberOfDependents'] # Excluding NumberOfTime60-89DaysPastDueNotWorse' because of strong collinearity
    X = df.loc[:,df.columns.isin(X_attributes)]

    X_train, X_test, y_train, y_test = train_test_split(X.values, y.values, test_size=0.20, random_state=42)

    xgb_model = xgb.XGBClassifier(objective="binary:logistic",random_state=42)

    xgb_model.fit(X_train,y_train.ravel())
    
    return xgb_model

# Save Model
def process_gcs_uri(uri: str) -> (str, str, str, str):
    '''
    Receives a Google Cloud Storage (GCS) uri and breaks it down to the scheme, bucket, path and file
    
            Parameters:
                    uri (str): GCS uri

            Returns:
                    scheme (str): uri scheme
                    bucket (str): uri bucket
                    path (str): uri path
                    file (str): uri file
    '''
    url_arr = uri.split("/")
    if "." not in url_arr[-1]:
        file = ""
    else:
        file = url_arr.pop()
    scheme = url_arr[0]
    bucket = url_arr[2]
    path = "/".join(url_arr[3:])
    path = path[:-1] if path.endswith("/") else path
    
    return scheme, bucket, path, file

def pipeline_export_gcs(fitted_pipeline: xgb.XGBClassifier, model_dir: str) -> str:
    '''
    Exports trained pipeline to GCS
    
            Parameters:
                    fitted_pipeline (sklearn.pipelines.Pipeline): the Pipeline object with data already fitted (trained pipeline object)
                    model_dir (str): GCS path to store the trained pipeline. i.e gs://example_bucket/training-job
            Returns:
                    export_path (str): Model GCS location
    '''
    scheme, bucket, path, file = process_gcs_uri(model_dir)
    if scheme != "gs:":
            raise ValueError("URI scheme must be gs")
    
    # Upload the model to GCS
    b = storage.Client().bucket(bucket)
    export_path = os.path.join(path, 'model.bst')
    blob = b.blob(export_path)
    fitted_pipeline.save_model('model.bst')
    blob.upload_from_filename('model.bst')
    return scheme + "//" + os.path.join(bucket, export_path)

# Define all the command line arguments your model can accept for training
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    # Input Arguments

    ''' 
    Vertex AI automatically populates a set of environment varialbes in the container that executes 
    your training job. those variables include:
        * AIP_MODEL_DIR - Directory selected as model dir
        * AIP_DATA_FORMAT - Type of dataset selected for training (can be csv or bigquery)
    
    Vertex AI will automatically split selected dataset into training,validation and testing
    and 3 more environment variables will reflect the locaiton of the data:
        * AIP_TRAINING_DATA_URI - URI of Training data
        * AIP_VALIDATION_DATA_URI - URI of Validation data
        * AIP_TEST_DATA_URI - URI of Test data
        
    Notice that those environment varialbes are default. If the user provides a value using CLI argument,
    the environment variable will be ignored. If the user does not provide anything as CLI  argument
    the program will try and use the environemnt variables if those exist. otherwise will leave empty.
    '''   
    parser.add_argument(
        '--model_dir',
        help = 'Directory to output model and artifacts',
        type = str,
        default = os.environ['AIP_MODEL_DIR'] if 'AIP_MODEL_DIR' in os.environ else ""
    )
    parser.add_argument(
        '--data_format',
        choices=['csv', 'bigquery'],
        help = 'format of data uri csv for gs:// paths and bigquery for project.dataset.table formats',
        type = str,
        default =  os.environ['AIP_DATA_FORMAT'] if 'AIP_DATA_FORMAT' in os.environ else "csv"
    )
    parser.add_argument(
        '--training_data_uri',
        help = 'location of training data in either gs:// uri or bigquery uri',
        type = str,
        default =  os.environ['AIP_TRAINING_DATA_URI'] if 'AIP_TRAINING_DATA_URI' in os.environ else ""
    )
    
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                    action="store_false")

    
    
    args = parser.parse_args()
    arguments = args.__dict__
    

    if args.verbose:
        logging.basicConfig(level=logging.INFO)
        
        
    logging.info('Model artifacts will be exported here: {}'.format(arguments['model_dir']))
    logging.info('Data format: {}'.format(arguments["data_format"]))
    logging.info('Training data uri: {}'.format(arguments['training_data_uri']) )
    
    
    '''
    We have 2 different ways to load our data to pandas. One is from cloud storage by loading csv files and
    the other is by connecting to BigQuery. Vertex AI supports both and 
    here we created a code that depelnding on the dataset provided, we will select the appropriated loading method.
    '''
    logging.info('Loading {} data'.format(arguments["data_format"]))
    if(arguments['data_format']=='csv'):
        df_train = load_data_from_gcs(arguments['training_data_uri'])
    elif(arguments['data_format']=='bigquery'):
        print(arguments['training_data_uri'])
        df_train = load_data_from_bq(arguments['training_data_uri'])
    else:
        raise ValueError("Invalid data type ")
    
    logging.info('Defining model parameters')    
    model_params = dict()
  
    logging.info('Running Model Training')   

    logging.info('Training Models Sklearn Isolation Forest')   
    XgBoost = train_model(model_params, df_train)

    logging.info('Export trained pipeline and report')   
    pipeline_export_gcs(XgBoost, arguments['model_dir'])

    logging.info('Training job completed. Exiting...')